<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\Major;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }	
    public function list(Request $request){
        
        return view( 'pages.manager.list' );
    }
}
