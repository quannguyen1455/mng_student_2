<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Spipu\Html2Pdf\Html2Pdf;

class PdfController extends Controller
{
    public function print_pdf() {
        return view('pages.course.bill-pdf');
    }
    public function down_pdf() {

        $html = "<style type=\"text/css\">
<!--
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    h4 {margin-left: 150px}
    div.content{
        border: 1px solid black;
        width: 500px;
        padding: 10px;
        margin-left: 50px;;
    }
    div.contain{
        border: 2px solid black;
        width: 100%;
        padding: 10px;
    }
-->
</style>
<page backtop=\"14mm\" backbottom=\"14mm\" backleft=\"10mm\" backright=\"10mm\" style=\"font-family: freeserif\">
    <b>TRUNG TÂM PHÁT TRIỂN GIÁO DỤC TINH ANH VIỆT</b><br>
    23 Đường số 9, Cư xá Bình Thới, PB, Q11<br>
    ĐT: 028.66542665 / DĐ: 090147986 <b>HĐS : MK015224</b>
    <h4 style=\"width: 100%; text-align: center\">PHIẾU THANH TOÁN</h4>
    <div class='content'>
        Họ tên : Nguyễn Nguyên Bảo<br>
        Điện thoại : <br>
        Lớp hiện đang học<br><br>
        <b>Danh sách các khoản thu</b><br><br>
        <div class='contain'>
            <table>
                <tr>
                    <th style='width: 120px;'>Lớp</th>
                    <th style='width: 180px;'>Giáo viên</th>
                    <th style='width: 120px;'>Học phí</th>
                    <th>Ngày đóng học phí<br>tháng tiếp theo</th>
                </tr>
                
                <tr>
                    <td>__</td>
                    <td>__</td>
                    <td>__</td>
                    <td>__</td>
                </tr>
            </table>
             - Số vở tặng : 0<br>
             - Nội dung đóng : học phí tháng 10
        </div>
        <b>Tổng cộng: 0 VNĐ<br>Ghi chú: </b><Br>
        - HS hoàn tất học phí từ ngày 01 đến ngày 06 mỗi tháng.<br>
        - PH-HS phải giữ phiếu thu học phí để theo dõi.<br>
        Nếu PH-HS làm mất giấy thu học phí thì trung tâm sẽ không giải quyết <br>
        - Học phí đóng rồi miễn trả lại.
    </div>
</page>";
        $html2pdf = new Html2Pdf('P','A4','en', 'UTF-8');
        $html2pdf->writeHTML($html);
        $html2pdf->output();
    }
        
    
}
