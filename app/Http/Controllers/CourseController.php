<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Personnel;
use App\Models\Classes;
use App\Models\Week;
use Illuminate\Http\Request;

class CourseController extends Controller
{   
    /**
     * [__construct description]
     */
	public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the list course.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function list(Request $request){
        $classes = Classes::all(['id', 'name'])->toArray();
        $personnel = Personnel::all(['id', 'first_name','last_name'])->toArray();
        $weeks = Week::all(['id','name'])->toArray();
        return view( 'pages.course.list', ['classes' => $classes, 'personnel' => $personnel, 'weeks'=> $weeks] );
    }
}
