<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Guardian;
use App\Models\Classes;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

 	/**
     * Show the list student.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function list(Request $request){
        $classes = Classes::all(['id', 'name'])->toArray();
        return view( 'pages.student.list', ['classes' => $classes] );
    }
}
