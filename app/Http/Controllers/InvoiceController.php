<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Personnel;
use App\Models\Classes;
use App\Models\Student;
use App\Models\InvoiceDetail;

use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function list(Request $request){
        $student = Student::all(['id', 'full_name','sex','birth_day','class_id','social_network','guardian_name','phone_number','note','liabilities'])
        ->toArray();
        $course = Course::all(['id', 'name','tuition'])->toArray();
        $invoice_detail = InvoiceDetail::all(['id','invoice_id','fee_id','course_id','price','expired','note'])->toArray();

        return view( 'pages.invoice.list',[ 'student'=>$student,'course'=>$course,'invoice_detail'=>$invoice_detail ]);

    }
}
