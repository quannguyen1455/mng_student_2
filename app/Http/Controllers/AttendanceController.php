<?php

namespace App\Http\Controllers;
use App\Models\Classes;
use App\Models\Student;
use App\Models\Attendance;
use App\Models\Personnel;
use App\Models\Course;
use Illuminate\Http\Request;


class AttendanceController extends Controller
{
 
  // public function list(){
  //        $atten = Attendance::with('class','student','course','personnel')->orderBy('id')->get()->toArray();
  //        dd($atten);
  // }
  public function list(Request $request){
    $classes = Classes::all(['id', 'name'])->toArray();
    $personnels = Personnel::all(['id', 'first_name','last_name'])->toArray();
    $students = Student::all(['id', 'full_name'])->toArray();
    $courses = Course::all(['id','name'])->toArray();
    // dd($classes,$personnels,$students,$courses);

    return view('pages.attendance.list', ['classes' => $classes, 'personnels' => $personnels, 'courses'=> $courses, 'students'=> $students] );
}

}