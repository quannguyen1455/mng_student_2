<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
    public function get_Mail() {
        return view('pages.course.mail.form');   
   }

  public function post_Mail(Request $request) {
        
        $request ->validate([
             'name' => 'required',
             'message' => 'required|min:10',
             'to_mail' => 'required|email'
        ],[
             'name.required' => 'Họ tên không được rỗng',
             'message.required' => 'Tin nhắn không được rỗng',
             'message.min' => 'Vui lòng nhập tin nhắn trên 10 kí tự',
             'to_mail.required' => 'Người nhận không được rỗng',
             'to_mail.email' =>'Vui lòng nhập đúng định dạng "examble@gmail.com" '

        ]);

        $data = [
             'name' => $request->input('name'),
             'mess' => $request->input('message'),
             'to_mail' => $request->input('to_mail')
        ];
        $to_mail = $request ->input('to_mail');
        Mail::send('pages.course.mail.mess', $data , function($msg) {
            $msg->from('lxtopvipxl@gmail.com','');
            $msg->to('cuongnguyen261298@gmail.com','');
            $msg->subject('Thư gửi');
      
        });
        echo "<script>
                  alert('Gửi thành công!');
                  window.location = '".url('/get-mail')."';
             </script>";
   }

}
