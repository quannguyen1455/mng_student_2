<?php

namespace App\Http\Controllers;

use App\Models\Major;
use App\Models\Personal;
use App\Models\Work;
use Illuminate\Http\Request;

class PersonnelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list(Request $request){
        $majors = Major::all(['id', 'name'])->toArray();
        $works = Work::all(['id', 'name'])->toArray();
        return view( 'pages.personnel.list', ['majors' => $majors, 'works' => $works] );
    }
}
