<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
	/**
	 * [Json return json data]
	 * @param [type]  $status [description]
	 * @param [type]  $data   [description]
	 * @param boolean $type   [description]
	 */
 	public function Json($status, $data, $type = false){
 		echo json_encode(array(
 			'status' => $status,
 			'data' => $data
 		), $type);
 		exit;
 	}

}
