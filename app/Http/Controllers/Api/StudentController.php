<?php
namespace App\Http\Controllers\Api;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Exports\StudentExport;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends ApiController
{
 	/**
     * Show the list student.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $students = Student::with('class')->orderBy('id')->get();
        $this->Json(true, $students);
    }

    /**
     * [detail Get detail of Student]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function detail(Request $request, $id){
    	$student = Student::where('id', $id)->first();
    	$this->Json(true, $student);
    }

    /**
     * [create Insert new student]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request){
    	$data = $request->input();
    	try{
    		if(!empty($data)){
    			$student = new Student;
    			$student->full_name = $data['full_name'];
    			$student->sex = $data['sex'];
    			$student->birth_day = $data['birth_day'];
    			$student->phone_number = $data['phone_number'];
    			$student->class_id = $data['class_id'];
    			$student->social_network = $data['social_network'];
    			$student->guardian_name = $data['guardian_name'];
    			$result = $student->save();

    			$this->Json($result, $student);
    		}else{
    			throw new \Exception();
    		}
    	}catch(\Exception $ex){
    		$this->Json(false, ['msg' => 'Create failed!']);
    	}
    }

    /**
     * [update Update student by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request){
    	try{
    		$studentId = $request->input('student_id');
    		if(!empty($studentId)){
    			$student = Student::where('id', $studentId)->first();
    			if(!empty($student)){
    				$data = $request->input();
    				$student->full_name = $data['full_name'];
	    			$student->sex = $data['sex'];
	    			$student->birth_day = $data['birth_day'];
	    			$student->phone_number = $data['phone_number'];
	    			$student->class_id = $data['class_id'];
	    			$student->social_network = $data['social_network'];
	    			$student->guardian_name = $data['guardian_name'];
	    			$result = $student->save();

	    			$this->Json($result, $student);
    			}else{
    				throw new \Exception();
    			}
    		}else{
    			throw new \Exception();
    		}
    	}catch(\Exception $ex){
    		$this->Json(false, ['msg' => 'Update failed!']);
    	}
    }

    /**
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request, $id){
        try{
            if(!empty($id)){
                $result = Student::findOrFail($id)->delete();
                $this->Json($result, []);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }

    public function export() 
    {
        return Excel::download(new StudentExport, 'student.xlsx');
    }    
}
