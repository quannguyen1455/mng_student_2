<?php

namespace App\Http\Controllers\Api;

use App\Models\Major;
use App\Models\Personnel;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class PersonnelController extends ApiController
{
    public function index()
    {
        $personnel = Personnel::with('work')->orderBy('id')->get();
        foreach ($personnel as $key => $value){
            if($value['major']=='Giáo Viên'){
                $value['children'] = json_decode(DB::table('courses')
                    ->select('courses.name', 'courses.tuition', 'courses.opening_day', 'courses.school_day', 'courses.class_room')
                    ->where('courses.teacher_id', '=', $value['id'])
                    ->get());
            }
        }
        $this->Json(true, $personnel);
    }

    public function detail(Request $request, $id){
        $personnel = Personnel::where('id', $id)->first();
        $this->Json(true, $personnel);
    }

    public function create(Request $request){
        $data = $request->input();
        try{
            if(!empty($data)){
                $personnel = new Personnel();
                $personnel->first_name = $data['first_name'];
                $personnel->last_name = $data['last_name'];
                $personnel->sex = $data['sex'];
                $personnel->major = $data['major'];
                $personnel->birth_day = $data['birth_day'];
                $personnel->work_id = $data['work_id'];
                $personnel->network_social = $data['network_social'];
                $personnel->email = $data['email'];
                $result = $personnel->save();

                $this->Json($result, $personnel);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Create failed!']);
        }
    }

    public function update(Request $request){
        try{
            $personnelId = $request->input('personnel_id');
            if(!empty($personnelId)){
                $personnel = Personnel::where('id', $personnelId)->first();
                if(!empty($personnel)){
                    $data = $request->input();
                    $personnel->first_name = $data['first_name'];
                    $personnel->last_name = $data['last_name'];
                    $personnel->email = $data['email'];
                    $personnel->sex = $data['sex'];
                    $personnel->birth_day = $data['birth_day'];
                    $personnel->major = $data['major'];
                    $personnel->work_id = $data['work_id'];
                    $personnel->network_social = $data['network_social'];
                    $result = $personnel->save();

                    $this->Json($result, $personnel);
                }else{
                    throw new \Exception();
                }
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Update failed!']);
        }
    }

    /**
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request, $id){
        try{
            if(!empty($id)){
                $result = Personnel::findOrFail($id)->delete();
                $this->Json($result, []);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }
}
