<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function index()
    {   
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $str = substr(str_shuffle($permitted_chars), 0, 10);
        return Excel::download(new UsersExport, 'users.xlsx');
        
    }
}
