<?php
namespace App\Http\Controllers\Api;

use App\Models\Admin;
use Illuminate\Http\Request;
use Hash;
use DB;
use App\Http\Controllers\Api\ApiController;

class ManagerController extends ApiController
{
 	/**
     * Show the list manager.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){
        $managers = Admin::with('major')->orderBy('id')->get();
        $this->Json(true, $managers);
    }

    /**
     * [detail Get detail of manager]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function detail(Request $request, $id){
    	$manager = Admin::where('id', $id)->first();
        $this->Json(true, $manager);
    
    }
    /**
     * [create Insert new manager]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request){
    	$data = $request->input();
    	// try{
    		if(!empty($data)){
    			$manager = new Admin;
                $manager->name = $data['name'];
                $manager->password = Hash::make($data['password']);
    			$manager->username = $data['username'];
    			$manager->type = $data['type'];
    			$manager->active = $data['active'];
    			$result = $manager->save();

    			$this->Json($result, $manager);
    		}else{
    			throw new \Exception();
    		}
    	// }catch(\Exception $ex){
    		// $this->Json(false, ['msg' => 'Create failed!']);
    	// }
    }

    /**
     * [update Update manager by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request){
    	try{
    		$managerId = $request->input('manager_id');
    		if(!empty($managerId)){
    			$manager = Admin::where('id', $managerId)->first();
    			if(!empty($manager)){
    				$data = $request->input();
                    $manager->name = $data['name'];
                    $manager->password = $data['password'];
	    			$manager->username = $data['username'];
	    			$manager->type = $data['type'];
	    			$manager->active = $data['active'];
	    			$result = $manager->save();

	    			$this->Json($result, $manager);
    			}else{
    				throw new \Exception();
    			}
    		}else{
    			throw new \Exception();
    		}
    	}catch(\Exception $ex){
    		$this->Json(false, ['msg' => 'Update failed!']);
    	}
    }

    /**
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request, $id){
        try{
            if(!empty($id)){
                $result = Admin::findOrFail($id)->delete();
                $this->Json($result, []);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }

    
}
