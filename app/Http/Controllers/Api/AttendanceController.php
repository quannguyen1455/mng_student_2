<?php
namespace App\Http\Controllers\Api;

use App\Models\Attendance;
use App\Models\Classes;
use App\Models\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;

class AttendanceController extends ApiController
{
 	/**
     * Show the list attendance.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request) {
        $attendances = Attendance::with('personnel')
        ->with('class')
        ->with('course')
        ->with('student')->orderBy('id')->get();

        $this->Json(true, $attendances);
    }

    /**
     * [detail Get detail of attendance]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function detail(Request $request, $id){
    	$attendance = Attendance::where('id', $id)->first();
        $this->Json(true, $attendance);
    
    }
    /**
     * [create Insert new attendance]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request){
    	$data = $request->input();
    	// try{
    		if(!empty($data)){
    			$attendance = new Attendance;
    			$attendance->status = $data['status'];
                $attendance->class_id = $data['class_id'];
                $attendance->student_id = $data['student_id'];
                $attendance->course_id = $data['course_id'];
                $attendance->teacher_id = $data['teacher_id'];
    			$attendance->note = $data['note'];
    			$result = $attendance->save();

    			$this->Json($result, $attendance);
    		}else{
    			throw new \Exception();
    		}
    	// }catch(\Exception $ex){
    	// 	$this->Json(false, ['msg' => 'Create failed!']);
    	// }
    }

    /**
     * [update Update attendance by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request){
    	try{
    		$attendanceId = $request->input('attendance_id');
    		if(!empty($attendanceId)){
    			$attendance = Attendance::where('id', $attendanceId)->first();
    			if(!empty($attendance)){
    				$data = $request->input();
                    $attendance->status = $data['status'];
                    $attendance->note = $data['note'];
	    			$result = $attendance->save();

	    			$this->Json($result, $attendance);
    			}else{
    				throw new \Exception();
    			}
    		}else{
    			throw new \Exception();
    		}
    	}catch(\Exception $ex){
    		$this->Json(false, ['msg' => 'Update failed!']);
    	}
    }

    /**
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request, $id){
        try{
            if(!empty($id)){
                $result = Attendance::findOrFail($id)->delete();
                $this->Json($result, []);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }

    

    
}
