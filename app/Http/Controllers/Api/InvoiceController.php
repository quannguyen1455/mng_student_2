<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Classes;
use App\Models\Student;
use App\Models\Personnel;
use App\Http\Request\CourseRequest;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use Spipu\Html2Pdf\Html2Pdf;

class InvoiceController extends ApiController
{
    public function index(Request $request) {
        $student = Student::with('class')->with('invoice')->get();
        $this->Json(true, $student);

    }

    public function create(Request $request){
    	$data = $request->input();
    	// try{
    		if(!empty($data)){
    			$invoice = new Student;
    			$invoice->invoice_date = $data['invoice_date'];
    			$invoice->student_id = $data['student_id'];
    			$invoice->content = $data['content'];
                $invoice->note = $data['note'];
                $invoice->biller = $data['biller'];
    			$result = $invoice->save();

    			$this->Json($result, $invoice);
    		}else{
    			throw new \Exception();
    		}
    	// }catch(\Exception $ex){
    	// 	$this->Json(false, ['msg' => 'Create failed!']);
    	// }
    }

    public function update(Request $request){
    	try{
    		$invoiceId = $request->input('invoice_id');
    		if(!empty($invoiceId)){
    			$invoice = Invoice::where('id', $invoiceId)->first();
    			if(!empty($invoice)){
    				$data = $request->input();
                    $invoice->invoice_date = $data['invoice_date'];
                    $invoice->student_id = $data['student_id'];
                    $invoice->content = $data['content'];
                    $invoice->note = $data['note'];
                    $invoice->biller = $data['biller'];
                    $result = $invoice->save();

	    			$this->Json($result, $invoice);
    			}else{
    				throw new \Exception();
    			}
    		}else{
    			throw new \Exception();
    		}
    	}catch(\Exception $ex){
    		$this->Json(false, ['msg' => 'Update failed!']);
    	}
    }

    public function delete(Request $request, $id){
        try{
            if(!empty($id)){
                InvoiceDetail::where('invoice_id',$id)->delete();
                $result = InvoiceDetail::findOrFail($id)->delete();
                $this->Json($result, []);
            }else{
                throw new \Exception();
            }
        }catch(\Exception $ex){
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }

    /**
     * Dùng html2pdf
     */

    public function inpdf(Request $request) {
        $student = Student::all(['id', 'full_name','sex','birth_day','class_id','social_network','guardian_name','phone_number','note','liabilities'])
        ->toArray();
        $course = Course::all(['id', 'name','tuition'])->toArray();
        $invoice_detail = InvoiceDetail::all(['id','invoice_id','fee_id','course_id','price','expired','note'])->toArray();
        
        $html = "<style type=\"text/css\">
        <!--
            h1 {color: #000033}
            h2 {color: #000055}
            h3 {color: #000077}
            h4 {margin-left: 150px}
            div.content{
                border: 1px solid lightblue;
                width: 500px;
                padding: 10px;
                margin-left: 50px;;
            }
            div.contain{
                border: 1px solid lightblue;
                width: 100%;
                padding: 10px;
            }
        -->
        </style>
        <page backtop=\"14mm\" backbottom=\"14mm\" backleft=\"10mm\" backright=\"10mm\" style=\"font-family: freeserif\">
            <b>TRUNG TÂM PHÁT TRIỂN GIÁO DỤC TINH ANH VIỆT</b><br>
            23 Đường số 9, Cư xá Bình Thới, PB, Q11<br>
            ĐT: 028.66542665 / DĐ: 090147986 <b> HĐS : MK015224</b>
            <h4 style=\"width: 100%; text-align: center\">PHIẾU THANH TOÁN</h4>
            <div class='content'>
                Họ tên:<br>
                Điện thoại: <br>
                Lớp hiện đang học: $ <br>
                <br>
                <b>Danh sách các khoản thu</b><br><br>
                <div class='contain'>
                    <table>
                        <tr>
                            <th style='width: 120px;'>Lớp</th>
                            <th style='width: 180px;'>Giáo viên</th>
                            <th style='width: 120px;'>Học phí</th>
                            <th>Ngày đóng học phí<br>tháng tiếp theo </th>
                        </tr>
                        
                        <tr>
                            <td>$</td>
                            <td>$</td>
                            <td>$</td>
                            <td>$.now(dd/mm/yy)</td>
                        </tr>
                    </table>
                    - Số vở tặng : 0<br>
                    - Nội dung đóng : học phí tháng 10
                </div>
                <b>Tổng cộng: 0 VNĐ<br>Ghi chú: </b><Br>
                - HS hoàn tất học phí từ ngày 01 đến ngày 06 mỗi tháng.<br>
                - PH-HS phải giữ phiếu thu học phí để theo dõi.<br>
                Nếu PH-HS làm mất giấy thu học phí thì trung tâm sẽ không giải quyết <br>
                - Học phí đóng rồi miễn trả lại.
            </div>
        </page>";

        $html2pdf = new Html2Pdf('P','A4','en', 'UTF-8');
        $html2pdf->writeHTML($html);
        $html2pdf->output();
    }

    public function inpdfid(Request $request, $id) {
        $invoice_detail = InvoiceDetail::where('id',$id)->first();

        $invoice = Invoice::where('id',$invoice_detail->invoice_id,$id)->first();
        $student = Student::where('id',$invoice->student_id,$id)->first();
        $classes = Classes::where('id', $student->class_id, $id)->first();

        $course = Course::where('id',$invoice_detail->course_id,$id)->first();
        $personnel = Personnel::where('id',$course->teacher_id,$id)->first();
        
        return view('pages.invoice.invoices',
        ['invoice_detail'=>$invoice_detail ,'invoice'=>$invoice,'student'=>$student,'classes'=>$classes,'course'=>$course,'personnel'=>$personnel]);
        
    }
}