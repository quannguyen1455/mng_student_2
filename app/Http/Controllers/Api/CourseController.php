<?php

namespace App\Http\Controllers\Api;

use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Validator;
use PDF; 
use DB;
use App\Models\Course;
use App\Models\CourseWeek;
use Illuminate\Http\Request;
use App\Http\Request\CourseRequest;
use App\Http\Controllers\Api\ApiController;

class CourseController extends ApiController
{
 	/**
     * Show the list course.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $courses = Course::with('personnel')
        ->with('class')
        ->with('course_week')->orderBy('id')->get();

        $this->Json(true, $courses);
    }

    public function detail(Request $request, $id) {
        $course = Course::with('course_week')->where('id', $id)->first();
        $this->Json(true, $course);
    }

    /**
     * [create Insert new course]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request) {
        $data = $request->input();
        // $request ->validate([
        //     'name' => 'required|min:3',
        //     'tuition' => 'required',
        //     'link' => 'required',
        //     'ratio' => 'required',
        //     'class_room' => 'required',
        //     'teacher_id' => 'required',
        //     'opening_day' => 'required',
        //     'school_day' => 'required',
        //     'from_time[]' => 'date_format:H:i',
        //     'to_time[]' => 'date_format:H:i|after:from_time[]'
        // ],[
        //     'name.required' => 'Tên khóa học không được rỗng',
        //     'name.min' => 'Tên khóa học từ 3 kí tự',
        // ]);
    	try {
    		if (!empty($data)) {
    			$course = new Course;
    			$course->name = $data['name'];
    			$course->tuition = $data['tuition'];
    			$course->ratio = $data['ratio'];
    			$course->link = $data['link'];
    			$course->teacher_id = $data['teacher_id'];
    			$course->opening_day = $data['opening_day'];
                $course->school_day = $data['school_day'];
                $course->class_room = $data['class_room'];
                $result = $course->save();
                
                if (!empty($data['week'])) {
                    $weekArr = $data['week'];
                    $fromTimeArr = $data['from_time'];
                    $toTimeArr = $data['to_time'];
                    
                    foreach($weekArr as $value) {
                        $courseWeek = new CourseWeek();
                        $courseWeek->course_id = $course->id;
                        $courseWeek->week_id = $value;
                        $courseWeek->from_time = $fromTimeArr[$value];
                        $courseWeek->to_time = $toTimeArr[$value];                  
                        if($fromTimeArr[$value] < $toTimeArr[$value]) {
                            $courseWeek->save();

                        } else {                                
                            $this ->Json( false, ['msg' => 'Dữ liệu nhập chưa hợp lệ!'] );
                            
                        }
                    }
                }
               
    			$this->Json($result, $course);
    		} else {
    			throw new \Exception();
    		}
    	} catch(\Exception $ex) {
        	$this->Json(false, ['msg' => 'Create failed!']);
    	}
    }

    /**
     * [update Update course by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request) {
        try {
            $courseId = $request ->input('id');
            if(!empty($courseId)){
                $course = Course::where('id', $courseId)->first();
                if(!empty($course)){
                    $data = $request->input();
                    $course->name = $data['name'];
                    $course->tuition = $data['tuition'];
                    $course->ratio = $data['ratio'];
                    $course->link = $data['link'];
                    $course->teacher_id = $data['teacher_id'];
                    $course->opening_day = $data['opening_day'];
                    $course->school_day = $data['school_day'];
                    $course->class_room = $data['class_room'];
                    $result = $course->save();

                    $oldCourseweek = CourseWeek::where('course_id', $courseId);
                    $oldCourseweek->delete();

                    if (!empty($data['week'])) {
                        $weekArr = $data['week'];
                        $fromTimeArr = $data['from_time'];
                        $toTimeArr = $data['to_time'];
   
                        foreach($weekArr as $value) {
                            $courseWeek = new CourseWeek();
                            $courseWeek->course_id = $courseId;
                            $courseWeek->week_id = $value;
                            $courseWeek->from_time = $fromTimeArr[$value];
                            $courseWeek->to_time = $toTimeArr[$value];                   
                            
                            if($fromTimeArr[$value] < $toTimeArr[$value]) {
                                $courseWeek->save();

                            } else {                                
                                $this ->Json( false, ['msg' => 'Dữ liệu nhập chưa hợp lệ!'] );
                            }
                        }
                    }
                   
                    $this -> Json($result, $course);
                } else {
                    throw new \Exception();
                }
            } else {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
           $this ->Json( false, ['msg' => 'Update failed!']);
        }
    }

    /**
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request, $id) {
        try {
            if (!empty($id)) {
                CourseWeek::where('course_id', $id)->delete();
                $result = Course::findOrFail($id)->delete();
                $this->Json($result, []);
            } else {
                throw new \Exception();
            }
        } catch(\Exception $ex) {
            $this->Json(false, ['msg' => 'Delete failed!']);
        }
    }
}
