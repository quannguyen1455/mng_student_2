<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
	public $table = "classes";
	/**
	 * [student description]
	 * @return [type] [description]
	 */
    public function student(){
    	return $this->hasOne('App\Models\Student', 'id', 'class_id');
	}
		/**
	 * [course description]
	 * @return [type] [description]
	 */
    public function course(){
    	return $this->hasOne('App\Models\Course', 'id', 'class_room');
	}
	
	public function attendance(){
    	return $this->hasOne('App\Models\Attendance', 'id', 'class_id');
	}
}
