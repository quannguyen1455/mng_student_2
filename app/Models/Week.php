<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    public $table = "weeks";

    public function course_week(){
    	return $this->hasOne('App\Models\CourseWeek', 'week_id', 'id');
    }
}

