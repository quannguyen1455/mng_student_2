<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    public $table = 'invoice_details';
    public function fee(){
    	return $this->hasOne('App\Models\Fee','fee_id','id');
    }
}
