<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $table = "students";
	/**
	 * [class description]
	 * @return [type] [description]
	 */
    public function class(){
    	return $this->belongsTo('App\Models\Classes')->select('classes.*','classes.name as class_name');
    }

    public function course(){
    	return $this->belongsTo('App\Models\Course');
    }
    /**
     * [invoice description]
     * @return [type] [description]
     */
    public function invoice(){
        return $this->hasMany('App\Models\Invoice','student_id','id')
        ->join('invoice_details','invoice_details.invoice_id','=', 'invoices.id')
        ->join('fees','fees.id','=', 'invoice_details.fee_id') 
        ->join('courses','courses.id','=', 'invoice_details.course_id') 
        ->join('personnels', 'personnels.id', '=', 'courses.teacher_id')
        ->select('invoices.*','fees.*','invoice_details.*','courses.name as course_name','personnels.first_name as f_n','personnels.last_name as l_n');
        ;
    }
    
    public function attendance(){
    	return $this->hasOne('App\Models\Attendance', 'id', 'student_id');
	}
}
