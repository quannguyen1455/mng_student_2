<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public $table = 'invoices';

	/**
	 *student description]
	 * @return [type] [description]
	 */
	public function invoicedetail(){
		return $this->hasMany('App\Models\InvoiceDetail','invoice_id','id')
		->join('fees','fees.id','=', 'invoice_details.fees_id') 
		->join('courses','courses.id','=', 'invoice_details.course_id') 
		->join('personnel', 'personnel.id', '=', 'courses.teacher_id')
		->select('invoice_details.*','fees.name as fee_name','courses.name as course_name','personnel.first_name as f_n','personnel.last_name as l_n');
	}

}
