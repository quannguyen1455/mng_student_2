<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
	// public $table = "personnel";
	/**
	 * [major description]
	 * @return [type] [description]
	 */
    public function course(){
    	return $this->hasOne('App\Models\Course', 'id', 'teacher_id');
    }

    public function work(){
        return $this->belongsTo('App\Models\Work');
	}
	
	public function attendance(){
    	return $this->hasOne('App\Models\Attendance', 'id', 'teacher_id');
	}
}
