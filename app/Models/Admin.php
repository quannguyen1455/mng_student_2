<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Admin extends Model 
{
    public function major(){
    	return $this->belongsTo('App\Models\Major');
    }
}
