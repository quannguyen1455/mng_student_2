<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{

    protected $table = 'major';
    public function admins(){
    	return $this->hasOne('App\Models\Admin', 'id', 'major_id');
    }
}
