<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    public function student(){
        return $this->hasOne('App\Models\Personnel', 'id', 'work_id');
    }
}
