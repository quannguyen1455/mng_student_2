<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model{ 

    protected $table = 'attendance_teacher';

    
    public function student(){
    	return $this->belongsTo('App\Models\Student','student_id','id');
    }

    public function class(){
    	return $this->belongsTo('App\Models\Classes');
    }
    public function personnel(){
    	return $this->belongsTo('App\Models\Personnel','teacher_id','id');
    }

    public function course(){
    	return $this->belongsTo('App\Models\Course');
    }
}
