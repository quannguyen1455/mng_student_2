<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	public $table = "courses";

	/**
	 *class description]
	 * @return [type] [description]
	 */
    public function class(){
    	return $this->belongsTo('App\Models\Classes', 'class_room', 'id');
	}

	public function student(){
    	return $this->belongsTo('App\Models\Student');
	}
	/**
	 *personnel description]
	 * @return [type] [description]
	 */
    public function personnel(){
    	return $this->belongsTo('App\Models\Personnel', 'teacher_id', 'id');
	}

	/**
	 *course_week description]
	 * @return [type] [description]
	 */
    public function course_week(){
		return $this->hasMany('App\Models\CourseWeek', 'course_id','id')
		->join('weeks', 'weeks.id', '=', 'course_weeks.week_id')
        ->select('course_weeks.*', 'weeks.name as week_name');
	}
	
    public function invoicedetail(){
    	return $this->belongsTo('App\Models\InvoiceDetail', 'course_id', 'id');
	}
	public function attendance(){
    	return $this->hasOne('App\Models\Attendance', 'id', 'course_id');
	}
}
