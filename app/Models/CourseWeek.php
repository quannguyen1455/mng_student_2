<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseWeek extends Model
{
    public $table = "course_weeks";

    public function rules()
    {
        return [
            'from_time' => 'date_format:H:i A',
            'to_time' => 'after_or_equal:from_time|date_format:H:i A'
        ];
    }
}

