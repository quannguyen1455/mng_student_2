<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>text</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>  
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>  
</head>
<body>
<ul>
<li>li 0</li>
<li>li 1</li>
<li class="test">li 2</li>
<li>li 3</li>
<li>li 4</li>
<li>li 5</li>
</ul>
<script src="{{ asset('js/text/text.js') }}" defer ></script>
</body>
</html>