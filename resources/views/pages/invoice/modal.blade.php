
<div id="invoiceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="billModalLabel" aria-hidden="true">
    <div stylez  class="modal-dialog block">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="invoiceModalLabel">{{ __('') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
            </div>
            <div class="modal-body">
                <form method= "post">
                @csrf
                    <input type="hidden" name="id" value="" />
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="invoice_date">{{ __('Invoice_date*') }}</label>
                            <input type="datetime-local" class="form-control" id="invoice_date" name="invoice_date" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="student_id">{{ __('Student*') }}</label>
                            <select  class="form-control" id="student_id" name="student_id" required>
                                <option value="">{{ __('Chọn học sinh') }}</option>
                                @foreach ($student as $st)
                                    <option value="{{ $st['id'] }}">{{ $st['full_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="content">{{ __('Content*') }}</label>
                            <textarea class="form-control" id="content" name="content" required></textarea>
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="note">{{ __('Note*') }}</label>
                            <textarea class="form-control" id="note" name="note" required></textarea>
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="biller">{{ __('Biller*') }}</label>
                            <input type="text" class="form-control" id="biller" name="biller" required/>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>
