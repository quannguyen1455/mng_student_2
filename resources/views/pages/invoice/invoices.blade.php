<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Hóa đơn</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/generic.css') }}" />
  <script src="{{ URL::asset('js/generic.js') }}"></script>
</head>
    <body onload="invoiceNextMonth()"> 
    <!-- <p style="float:left; margin-left:20px"><button onClick="t()" type="button" class="btn btn-primary">Click</button></p>
    <div id="demo"></div> -->
    <p style="float:right; margin-right:20px"><button onClick="printPage()" type="button" class="btn btn-primary">In hóa đơn</button></p>
    <div class ="invoice" >
        <b>TRUNG TÂM PHÁT TRIỂN GIÁO DỤC TINH ANH VIỆT</b></br>
        23 Đường số 9, Cư xá Bình Thới, PB, Q11 </br>
        ĐT: 028.66542665 / DĐ: 090147986 <b> HĐS : MK015224</b></br>
                <h4 style="font-size:24px; margin-top:10px; text-align: center">PHIẾU THANH TOÁN</h4>
                <div>
                    <p>Họ tên : <b> {{ $student->full_name }} </b></p>
                    <p>Điện thoại:<b> {{ $student->phone_number }} </b></p>
                    <p>Lớp hiện đang học:<b> {{ $classes->name }} </b></p>
                    <p></p>
                    <p><b>Danh sách các khoản thu</b></p>
                    <div class='table table-bordered'>
                        <table>
                            <tr>
                                <th>Khóa học</th>
                                <th>Giáo viên</th>
                                <th>Học phí</th>
                                <th>Ngày đóng học phí tháng tiếp theo </th>
                            </tr>
                            
                            <tr>
                                <td><b>{{ $course->name }}</b></td>
                                <td><b>{{ $personnel->first_name }} {{ $personnel->last_name }}</b></td>
                                <td><b>{{ $invoice_detail->price }}</b></td>
                                <td>
                                    <b><div id="getNextMonth">
                                       
                                    </div></b>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <p>- Số vở tặng : 0</p>
                    <p>- Nội dung đóng : học phí tháng 10 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Tổng cộng: {{ $invoice_detail->price }} VNĐ</b></p>
                        
                    <p><b>Ghi chú: </b></p>
                    - HS hoàn tất học phí từ ngày 01 đến ngày 06 mỗi tháng.<br>
                    - PH-HS phải giữ phiếu thu học phí để theo dõi.<br>
                    Nếu PH-HS làm mất giấy thu học phí thì trung tâm sẽ không giải quyết <br>
                    - Học phí đóng rồi miễn trả lại.
    </div>
    </body>
</html>
<script src="{{ asset('js/invoice/invoice.js') }}" defer ></script>