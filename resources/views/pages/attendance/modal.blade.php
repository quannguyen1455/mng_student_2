
<div id="attendanceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="attendanceModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="attendanceModalLabel">{{ __('Thêm Học Viên') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            
                <form>
                    <input type="hidden" name="attendance_id" value="" />
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="class-id">{{ __('Hiện đang học*') }}</label>
                            <select class="form-control" id="class-id" name="class_id" required>
                                <option value="">{{ __('Chọn lớp') }}</option>
                                @foreach ($classes as $class)
                                    <option value="{{ $class['id'] }}">{{ $class['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="teacher-id">{{ __('Giao vien*') }}</label>
                            <select class="form-control" id="teacher-id" name="teacher_id" required>
                            <option value="">{{ __('__Chọn__') }}</option>
                                @foreach ($personnels as $personnel)
                                    <option value="{{ $personnel['id'] }}"> {{ $personnel['first_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="student-id">{{ __('student*') }}</label>
                            <select class="form-control" id="student-id" name="student_id" required>
                            <option value="">{{ __('__Chọn__') }}</option>
                                @foreach ($students as $student)
                                    <option value="{{ $student['id'] }}"> {{ $student['full_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="course-id">{{ __('khoa học*') }}</label>
                            <select class="form-control" id="course-id" name="course_id" required>
                                <option value="">{{ __('__Chọn__ ') }}</option>
                                @foreach ($courses as $course)
                                    <option value="{{ $course['id'] }}">{{ $course['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="form-row mb-1">
                        <label class="col-md-12"> {{ __('Status') }}</label>
                        <div class="form-group col-md-12 ml-5">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status-absent" value="absent" checked/>
                                <label class="form-check-label" for="status-male">
                                    {{ __('absent') }}
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status-resent" value="resent"/>
                                <label class="form-check-label" for="status-female">
                                    {{ __('resent') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="note">{{ __('note') }}</label>
                            <input class="form-control" type="text" id="note" name="note" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>
