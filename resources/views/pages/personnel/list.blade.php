@extends('layouts.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> {{ __('Danh Sách Nhân Viên') }}</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="index.html" class="text-muted"> {{ __('Quản Lý') }} </a></li>
                                <li class="breadcrumb-item text-muted active" aria-current="page">{{ __('Nhân Viên') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-5 d-flex align-self-end justify-content-end">
                    <div class="customize-input mr-3">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#personnelModal">
                        <span class="btn-label">
                            <i class="fas fa-plus"></i>
                        </span>
                            {{ __('Thêm') }}
                        </button>
                    </div>
                    <div class="customize-input mr-3">
                        <button class="btn btn-light" type="button">
                        <span class="btn-label">
                            <i class="far fa-trash-alt"></i>
                        </span>
                            {{ __('Xoá') }}
                        </button>
                    </div>
                    <div class="customize-input">
                        <button class="btn btn-warning" type="button">
                        <span class="btn-label">
                            <i class="fas fa-file-excel"></i>
                        </span>
                            {{ __('Xuất File') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- *************************************************************** -->
            <!-- Start Top Leader Table -->
            <!-- *************************************************************** -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="personnel-table" class="table table-striped table-bordered no-wrap">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Danh sách các lớp dạy</h4>
                            </div>
                            <div class="modal-body" id="nono"></div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- *************************************************************** -->
            <!-- End Top Leader Table -->
            <!-- *************************************************************** -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->

    @include('pages.personnel.modal', ['majors' => $majors, 'works' => $works])

@endsection

<script src="{{ asset('js/personnel/personnel.js') }}" defer></script>
