<div id="personnelModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="personnelModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="personnelModalLabel">{{ __('Thêm Học Viên') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form style="margin-left: 15px;">
                    <input type="hidden" name="personnel_id" value="" />
                    <div class="row">
                        <div class="form-row mb-1" style="margin-right: 10px;">
                            <div class="form-group col-md-12">
                                <label for="first_name">{{ __('Họ*') }}</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" required/>
                            </div>
                        </div>
                        <div class="form-row mb-1">
                            <div class="form-group col-md-12">
                                <label for="last_name">{{ __('Tên*') }}</label>
                                <input class="form-control" type="text" id="last_name" name="last_name" required/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-row mb-1" style="margin-right: 10px;">
                            <div class="form-group col-md-12">
                                <label>{{ __('Email*') }}</label>
                                <input class="form-control" type="email" id="email" name="email" placeholder="Nhập email nè bạn!" required/>
                            </div>
                        </div>
                        <div class="form-row mb-1">
                            <label class="col-md-12"> {{ __('Giới tính') }}</label>
                            <div class="form-group col-md-12 ml-5">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="sex-male" value="male" checked/>
                                    <label class="form-check-label" for="sex-male">
                                        {{ __('Nam') }}
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="sex-female" value="female"/>
                                    <label class="form-check-label" for="sex-female">
                                        {{ __('Nữ') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-row mb-1" style="margin-right: 35px;">
                            <div class="form-group col-md-12">
                                <label for="birth_day">{{ __('Ngày sinh*') }}</label>
                                <input class="form-control" type="date" id="birth_day" name="birth_day" required style="width: 220px;"/>
                            </div>
                        </div>
                        <div class="form-row mb-1">
                            <div class="form-group col-md-12">
                                <label for="major-id">{{ __('Hiện đang làm*') }}</label>
                                <select class="form-control" id="major-id" name="major" style="width: 220px; margin-left:-25px;" required>
                                    <option value="">{{ __('Chọn nghề nghiệp!') }}</option>
                                    @foreach ($majors as $major)
                                        <option value="{{ $major['name'] }}">{{ $major['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-row mb-1" style="margin-right: 15px;">
                            <div class="form-group col-md-12">
                                <label for="work-id">{{ __('Work id*') }}</label>
                                <select class="form-control" id="work_id" name="work_id" style="width: 220px;" required>
                                    <option value="">{{ __('Chọn Work id!') }}</option>
                                    @foreach ($works as $work)
                                        <option value="{{ $work['id'] }}">{{ $work['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row mb-1">
                            <div class="form-group col-md-12">
                                <label for="network-social">{{ __('Facebook') }}</label>
                                <input class="form-control" type="text" id="network_social" name="network_social" style="width: 215px;margin-left:-5px;" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>
