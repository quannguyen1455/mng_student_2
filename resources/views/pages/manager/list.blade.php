@extends('layouts.master')

@section('content')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> {{ __('Danh Sách Học Viên') }}</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="index.html" class="text-muted"> {{ __('Quản Lý') }} </a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">{{ __('Học Viên') }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 d-flex align-self-end justify-content-end">
                <div class="customize-input mr-3">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#managerModal">
                        <span class="btn-label">
                            <i class="fas fa-plus"></i>
                        </span>
                        {{ __('Thêm') }}
                    </button>
                </div>
                <div class="customize-input mr-3">
                    <button class="btn btn-light" type="button">
                        <span class="btn-label">
                            <i class="far fa-trash-alt"></i>
                        </span>
                        {{ __('Xoá') }}
                    </button>
                </div>
                <div class="customize-input">
                    <button class="btn btn-warning" type="button">
                        <span class="btn-label">
                            <i class="fas fa-file-excel"></i>
                        </span>
                        {{ __('Xuất File') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- *************************************************************** -->
        <!-- Start Top Leader Table -->
        <!-- *************************************************************** -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="manager-table" class="table table-striped table-bordered no-wrap">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- *************************************************************** -->
        <!-- End Top Leader Table -->
        <!-- *************************************************************** -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->

@include('pages.manager.modal')

@endsection

<script src="{{ asset('js/manager/manager.js') }}" defer></script>
