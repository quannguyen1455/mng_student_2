
<div id="managerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="managerModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="managerModalLabel">{{ __('Thêm Học Viên') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="manager_id" value="" />
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="name">{{ __('Name*') }}</label>
                            <input type="text" class="form-control" id="name" name="name" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="username">{{ __('UserName*') }}</label>
                            <input class="form-control" type="text" id="username" name="username" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label>{{ __('PassWord*') }}</label>
                            <input class="form-control" type="text" id="password" name="password" placeholder=""required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label>{{ __('Type*') }}</label>
                            <input class="form-control" type="number" id="type" name="type" placeholder="1"required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label>{{ __('Active*') }}</label>
                            <input class="form-control" type="number" id="active" name="active" placeholder="0"required/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>