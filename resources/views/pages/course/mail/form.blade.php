<!DOCTYPE html>
<html lang="en">
<head>
  <title>Send Mail</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="mail-icon/favicon.png"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

        <div class="container">
            <h2>Gửi mail</h2>
            <form action="/post-mail" method ="post">
            @csrf
                <div class="form-group col-md-6">
                <label for="name">Họ tên</label>
                <input type="text" class="form-control" id="name" name="name">
                @if($errors->has('name'))
                    <div class="col-md-6">
                        <strong class="text-danger"> {{ $errors ->first('name') }} </strong>
                    </div>
                @endif
                </div>
                <div class="form-group col-md-6">
                <label for="name">Gửi đến</label>
                <input type="email" class="form-control" id="to_mail" name="to_mail">
                @if($errors->has('to_mail'))
                    <div class="col-md-6">
                        <strong class="text-danger"> {{ $errors ->first('to_mail') }} </strong>
                    </div>
                @endif
                </div>
                <div class="form-group col-md-6">
                <label for="message">Lời nhắn</label>
                <textarea class="form-control" id="message" name="message"></textarea>
                @if($errors->has('message'))
                    <div class="col-md-6">
                        <strong class="text-danger"> {{ $errors ->first('message') }} </strong>
                    </div>
                @endif
                </div>
                <div class="modal-footer col-md-6">
                    <button type="reset" class="btn btn-primary">Xóa</button>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </div>
            </form>
        </div>

</body>
</html>
