<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>File Download</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <style >
    body {
    font-family: DejaVu Sans, sans-serif;
    }
    </style>
</head>
    <body>
        <h1>Tiêu đề</h1>
        <table>
            <thead>
                <tr>
                    <th>Số TT</th>
                    <th>Nội dung</th>
                </tr>
            </thead>
            <tbody>
                @foreach (range(1, 5) as $count)
                <tr>
                    <td>{{ $count }}</td>
                    <td>Nôi dụng của dòng thứ {{ $count }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>