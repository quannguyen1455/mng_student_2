
<div id="courseModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="courseModalLabel" aria-hidden="true">
    <div style class="modal-dialog block">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="courseModalLabel">{{ __('') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
            </div>
            <div class="modal-body">
                <form method= "post">
                @csrf
                    <input type="hidden" name="id" value="" />
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="name">{{ __('Tên khóa học*') }}</label>
                            <input type="text" class="form-control" id="name" name="name" required/>
                            @if($errors->has('name'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('name') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-12">
                            <label for="tuition">{{ __('Học phí*') }}</label>
                            <input class="form-control" type="number" id="tuition" name="tuition" required/>
                            @if($errors->has('tuition'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('tuition') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-6">
                            <label>{{ __('Tỉ lệ*') }}</label>
                            <input class="form-control" type="number" id="ratio" name="ratio" placeholder="" pattern="" required/>
                            @if($errors->has('ratio'))
                                <div class="col-md-6">  
                                    <strong class ="text-danger"> {{ $errors ->first('ratio') }} </strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="link">{{ __('Link') }}</label>
                            <input class="form-control" type="number" id="link" name="link" placeholder="" pattern="" required/>
                            @if($errors->has('link'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('link') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-6">
                            <label for="class_room">{{ __('Lớp*') }}</label>
                            <select class="form-control" id="class_room" name="class_room" required>
                                <option value="">{{ __('Chọn lớp') }}</option>
                                @foreach ($classes as $class)
                                    <option value="{{ $class['id'] }}">{{ $class['name'] }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('class_room'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('class_room') }} </strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="teacher-id">{{ __('Giáo viên chủ nhiệm*') }}</label>
                            <select class="form-control" id="teacher-id" name="teacher_id" required>
                            <option value="">{{ __('__Chọn__') }}</option>
                                @foreach ($personnel as $personnel)
                                    <option value="{{ $personnel['id'] }}">{{ $personnel['id'] }} - {{ $personnel['first_name'] }} {{ $personnel['last_name'] }}</option>
                                @endforeach
                            </select>
                            </select>
                            @if($errors->has('teacher_id'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('teacher_id') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-row mb-1 hide">
                        <div class="form-group col-md-6">
                            <label for="opening-day">{{ __('Ngày khai giảng*') }}</label>
                            <input class="form-control" type="date" id="opening_day" name="opening_day" required/>
                            @if($errors->has('opening_day'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('opening_day') }} </strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="school_day"> {{ __('Ngày bắt đầu đến trường*') }}</label>
                            <input class="form-control" type="date" name="school_day" id="school_day" value="" required/>
                            @if($errors->has('school_day'))
                                <div class="col-md-6">
                                    <strong class ="text-danger"> {{ $errors ->first('school_day') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-row mb-1">
                        <div class="form-group col-md-12" id ="block">
                            <label for="class_room">{{ __('Thời gian học*') }}</label><br/>
                            <div>
                                @foreach($weeks as $week)
                                    <div class="mb-1 col-md-12 mr-1">
                                        <span class="mb-1 col-md-5">
                                            <label for="week_{{ $week['id']}}" style="width:80px" >{{ $week['name'] }}</label>
                                            <input class ="hide" type="checkbox" name="week[]" id="week_{{ $week['id'] }}" value="{{ $week['id'] }}" require/>
                                            @if($errors->has('week'))
                                                <div class="col-md-6">
                                                    <strong class ="text-danger"> {{ $errors ->first('week') }} </strong>
                                                </div>
                                            @endif
                                        </span>
                                        <span class="mb-1 col-md-5">
                                            <input class="ml-3" type="time" name="from_time[{{ $week['id'] }}]" id="from_time" value="{{ $week['id'] }}" require/>
                                            @if($errors->has('from_time'))
                                                <div class="col-md-6">
                                                    <strong class ="text-danger"> {{ $errors ->first('from_time') }} </strong>
                                                </div>
                                            @endif
                                            <input class="ml-3" type="time" name="to_time[{{ $week['id'] }}]" id="to_time" value="{{ $week['id'] }}" require/>
                                            @if($errors->has('to_time'))
                                                <div class="col-md-6">
                                                    <strong class ="text-danger"> {{ $errors ->first('to_time') }} </strong>
                                                </div>
                                            @endif
                                        </span>
                                    </div> 
                                @endforeach   
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>
