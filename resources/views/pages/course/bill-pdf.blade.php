<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>File Download</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <style >
    body {
    font-family: DejaVu Sans, sans-serif;
    }
    .header {
        width : auto;   
        height : auto;
        font-size: 12px;
        margin-left:20px;
    }
    .content {
        font-size: 12px;
        width : 97%;
        padding : 10px 2px 2px 2px ;
        height : 500px;
        border: 2px solid black;
        margin-left: 20px;
    }
    .content1 {
        font-size: 12px;
        width : 22%;
        float: left;
        height : auto;
        margin-left:20px;

    }
    </style>
</head>
    <body>
        <div class ="header">
            <p><b>TRUNG TÂM PHÁT TRIỂN GIÁO DỤC TINH ANH VIỆT</b></p>
            <p>23 Đường số 9, Cư xá Bình Thới, P8, Q11</p>
            <p>ĐT: 0123456789</p>
            <p><b>HĐS:MK015224</b></p>
            <p style ="text-align:center; font-size: 26px"><b>PHIẾU THU HỌC PHÍ</b></p>
        </div>
        <div class ="content">
            <a style ="float:right" href ="/api/export/post_pdf" >
                <button name="" id="">Print</button>
            </a>
            <script>
            function a(){
                window.location.href = "/api/export/post_pdf";
            }
            </script>
            <p>Họ tên: </p>
            <p>Điện thoại: </p>
            <p>Lớp hiện đang học: </p>
            <p style ="font-size: 18px"><b>Danh sách khoản thu</b></p>
                <div class="content1">
                    <p><b>Lớp</b></p>
                    <p>__</p>
                </div>
                <div class="content1">
                    <p><b>Giáo viên</b></p>
                    <p>__</p>
                </div>
                <div class="content1">
                    <p><b>Học phí</b></p>
                    <p>__</p>
                </div>
                <div class="content1">
                    <p><b>Ngày đóng học phí tháng tiếp theo</b></p>
                    <p>__</p>
                </div>
            <br/><br/><br/><br/><br/>
            <p style ="margin-left:20px;"><i>-Sách vở tặng: ___</i></p>
            <p style ="margin-left:20px;"><i>-Nội dung đóng: ___</i></p>

            <p style ="float:right; font-size:18px"><b>Tổng cộng: ___ VND</b></p>
            <div class="content1">
                    <p>Ghi chú:</p>
                    <p>-</p>
                    <p>-</p>
                    <p>-</p>
            </div>
            <br/>
            <div class="content1">
                    <b>
                    <p>Ngày __ tháng __ năm __</p>
                    <p>Người thu</p>
                    <p>___</p>
                    </b>
            </div>
            <div class="content1">
                    <b>
                    <p></p>
                    <p>Người đóng học phí</p>
                    <p>___</p>
                    </b>
            </div>

        </div>
    </body>
</html>