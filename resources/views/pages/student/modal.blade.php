
<div id="studentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="studentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="studentModalLabel">{{ __('Thêm Học Viên') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="student_id" value="" />
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="full-name">{{ __('Họ và tên*') }}</label>
                            <input type="text" class="form-control" id="full-name" name="full_name" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="birth-day">{{ __('Ngày sinh*') }}</label>
                            <input class="form-control" type="date" id="birth-day" name="birth_day" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label>{{ __('Điện thoại*') }}</label>
                            <input class="form-control" type="tel" id="phone-number" name="phone_number" placeholder="032 456 798" pattern="[0-9]{3}[0-9]{3}[0-9]{3}" required/>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <label class="col-md-12"> {{ __('Giới tính') }}</label>
                        <div class="form-group col-md-12 ml-5">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" id="sex-male" value="male" checked/>
                                <label class="form-check-label" for="sex-male">
                                    {{ __('Nam') }}
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" id="sex-female" value="female"/>
                                <label class="form-check-label" for="sex-female">
                                    {{ __('Nữ') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="class-id">{{ __('Hiện đang học*') }}</label>
                            <select class="form-control" id="class-id" name="class_id" required>
                                <option value="">{{ __('Chọn lớp') }}</option>
                                @foreach ($classes as $class)
                                    <option value="{{ $class['id'] }}">{{ $class['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="social-network">{{ __('Facebook') }}</label>
                            <input class="form-control" type="text" id="social-network" name="social_network" />
                        </div>
                    </div>
                    <div class="form-row mb-1">
                        <div class="form-group col-md-12">
                            <label for="guardian-name">{{ __('Phụ Huynh*') }}</label>
                            <input class="form-control" type="text" id="guardian-name" name="guardian_name" required/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                <button type="button" id="modal--btn-submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>