<div class="alert-notification alert alert-success alert-dismissible bg-success text-white border-0 d-none" role="alert">
    {{ __('Thực hiện thành công !!!') }}
</div>

<div class="alert-notification alert alert-danger alert-dismissible bg-danger text-white border-0 d-none" role="alert">
    {{ __('Đã có lỗi xảy ra !!!') }}
</div>
