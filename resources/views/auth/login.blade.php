@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="owl mt-6">
            <div class="hand"></div>
            <div class="hand hand-r"></div>
            <div class="arms">
                <div class="arm"></div>
                <div class="arm arm-r"></div>
            </div>
        </div>
        <div class="form">
            <div class="control">
                <label for="username" class="fa fa-user"></label>
                <input id="username" type="text" placeholder="Tài Khoản" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="control">
                <label for="password" class="fa fa-asterisk"></label>
                <input id="password" name="password" placeholder="Mật khẩu" type="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="current-password" />
                @error('password' )
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-12 d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Đăng Nhập') }}
                    </button>
                </div>
            </div>
        </div>
        
    </form>
</div>

@endsection
