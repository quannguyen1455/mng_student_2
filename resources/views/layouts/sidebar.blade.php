<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{ url('/') }}" aria-expanded="false">
                        <i data-feather="home" class="feather-icon"></i>
                        <span class="hide-menu">{{ __('Trang Chủ') }}</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                <li class="nav-small-cap"><span class="hide-menu">{{ __('Quản Lý') }}</span></li>

                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{ url('/student/list') }}" aria-expanded="false">
                        <i class="fas fa-child"></i>
                        <span class="hide-menu"> {{ __('Học Viên') }} </span>
                    </a>
                </li>
               
                <li class="sidebar-item"> 
                    <a class="sidebar-link sidebar-link" href="{{ url('/course/list') }}" aria-expanded="false">
                        <i class="fas fa-object-group"></i>
                        <span class="hide-menu"> {{ __('Khoá Học') }}</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{ url('personnel/list') }}" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        <span class="hide-menu"> {{ __('Nhân Viên') }}</span>
                    </a>
                </li>

                <li class="sidebar-item"> 
                    <a class="sidebar-link sidebar-link" href="{{ url('/invoice/list') }}" aria-expanded="false">
                        <i class="far fa-money-bill-alt"></i>
                        <span class="hide-menu"> {{ __('Hoá Đơn') }}</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                <li class="nav-small-cap">
                    <span class="hide-menu"> {{ __('Điểm Danh') }} </span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{ url('/attendance/list') }}" aria-expanded="false">
                        <i class="fas fa-calendar-alt"></i>
                        <span class="hide-menu"> {{ __('Khoá Học Trong Ngày') }}</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                <li class="nav-small-cap">
                    <span class="hide-menu"> {{ __('Dịch Vụ') }} </span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="authentication-login1.html" aria-expanded="false">
                        <i class="fas fa-warehouse"></i>
                        <span class="hide-menu"> {{ __('Bán Trú') }}</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="authentication-login1.html" aria-expanded="false">
                        <i class="fas fa-newspaper"></i>
                        <span class="hide-menu"> {{ __('Báo Đài') }}</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                <li class="nav-small-cap">
                    <span class="hide-menu"> {{ __('Hệ Thống') }} </span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="authentication-login1.html" aria-expanded="false">
                        <i class="fas fa-history"></i>
                        <span class="hide-menu"> {{ __('Lịch Sử Đăng Nhập') }}</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link sidebar-link" href="{{ url('/manager/list') }}" aria-expanded="false">
                        <i class=" fas fa-user"></i>
                        <span class="hide-menu"> {{ __('Danh Sách User') }}</span>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div> 
    <!-- End Sidebar scroll-->   
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
