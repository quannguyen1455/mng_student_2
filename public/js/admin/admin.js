$(function () {
	$("#admin-table").renderTable('/list', [
		{ title: 'name', data: null, render: (data, type, row, meta) => `${row.name}` },
		{ title: 'username', data: 'username', render: (data) => `${row.username}`},
		{ title: 'password', data: 'password', render: (data) => `${row.password}` },
		{ title: 'type', data: 'type', render: (data) => `${row.type}` },
		{ title: 'active', data: null, render: (data, type, row, meta) => `${row.active}` },
		{ title: '', data: null, orderable: false, render: (data) => { 
				return `
                        <span class="btn-label btn-edit" data-toggle="modal" data-target="#adminModal" data-type-modal="update" data-id="${data.id}">
                            <i class="far fa-edit"></i>
                        </span>
                        <span class="btn-label ml-2 btn-delete" onClick="deleteadmin(${data.id})">
                            <i class="fas fa-times-circle"></i>
                        </span>
	                    `;
			} 
		}
	]);

	$('#adminModal').on('show.bs.modal', function(evt){
		const self = $(this);
		const form = self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');
		
		if(typeModal === 'update'){
			const idAdmin = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/web//byId/${idAdmin}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					form.find('input[name="student_id"]').val(idStudent);
					form.find('input[name="full_name"]').val(data.full_name);
					form.find('input[name="birth_day"]').val(data.birth_day);
					form.find('input[name="phone_number"]').val(data.phone_number);
					form.find(`input[name="sex"][value="${data.sex}"]`).attr('checked', 'checked');
					form.find(`select[name="class_id"] option[value="${data.class_id}"]`).attr('selected', 'selected');
					form.find('input[name="social_network"]').val(data.social_network);
					form.find('input[name="guardian_name"]').val(data.guardian_name);
				}
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const self = $(this);
		self.resetFormOfModal();
		const form = self.find('form');
		form.find('input[name="student_id"]').val('');
		form.find('input[name="sex"][value="male"]').attr('checked', 'checked');
		form.find('select[name="class_id"] option[value=""]').attr('selected', 'selected');
	});

	$('#studentModal').find('#modal--btn-submit').on('click', function(evt){
		const studentId = $('#studentModal').find('form').find('input[name="student_id"]').val();

		let url = 'student/create';
		if(studentId){
			url = 'student/update';
		}

		$('#studentModal').submitFormModalWithAjax(url, {
			messages: {
				full_name: 'Họ và tên không được bỏ trống!',
				birth_day: 'Vui lòng chọn ngày tháng năm sinh!',
				phone_number: 'Số điện thoại không được bỏ trống!',
				class_id: 'Vui lòng chọn lớp!',
				guardian_name: 'Tên phụ huynh không được bỏ trống!'
			}
		}, 'student-table', 'student/list');
	});
}(jQuery));

/**
 * [deleteStudent description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function deleteStudent(id){
	$.ajax({
		url: `/api/student/delete/byId/${id}`,
		method: 'GET',
		dataType: 'json'
	}).done(function(response){
		if(response.status){
			alertSuccess();
			$("#student-table").refreshTable('student/list');
		}else{
			alertError();
			console.log('Error render table: ' + response.msg);
		}
	}).fail(function(error){
		alertError();
		console.log('Error render table: ' + error);
	});
}