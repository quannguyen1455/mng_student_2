$(function () {
	/**
	 * [renderTable Render table]
	 * @param  {[type]} url     [description]
	 * @param  {[type]} columns [description]
	 * @return {[type]}         [description]
	 */
	$.fn.renderTable = function (url, columns){
		const self = this;

		$.ajax({
			url: `${url}`,
			method: 'GET',
			dataType: 'json'
		}).done(function(response){
			if(response.status){
				self.DataTable({
					treeGrid: {
						left: 10,
						expandIcon: '<span>+</span>',
						collapseIcon: '<span>-</span>'
					},
					data: response.data,
					columns: columns,
					scrollY: 400,
					scrollCollapse: true,
					pageLength:10,
					bPaginate: true,
				    bLengthChange: true,
					bFilter: true,
					bInfo: true,
				    language: {
				    	emptyTable: 'Không có kết quả nào được tìm thấy !!!',
						search: 'Tìm Kiếm',
						info: "Tổng cộng",
					   	paginate: {
						  first :'Trang đầu',
						  last:'Trang cuối',
					      previous: '<',
					      next: '>'
					    },
					}
				});
			}else{
				console.error('Error render table: ' + response.msg);
			}
		}).fail(function(error){
			console.error('Error render table: ' + error);
		});
	};

	/**
	 * [refreshTable Refresh table]
	 * @param  {[type]} url [description]
	 * @return {[type]}     [description]
	 */
	$.fn.refreshTable = function (url){
		const $self = this.DataTable();
		$.ajax({
			url: `/api/${url}`,
			method: 'GET',
			dataType: 'json'
		}).done(function(response){
			if(response.status){
				$self.clear().rows.add(response.data).draw();
			}else{
				console.error('Error refresh table: ' + response.msg);
			}
		}).fail(function(error){
			console.error('Error refresh table: ' + error);
		});
	};

	/**
	 * [resetFormOfModal Reset all data from form of modal]
	 * @return {[type]} [description]
	 */
	$.fn.resetFormOfModal = function(){
		this.find('form').validate().resetForm();          // hàm resetForm có sẵn
		this.find('form')[0].reset();                      // hàm reset() có sẵn
		$(".error").removeClass("error");
	};

	/**
	 * [submitFormModalWithAjax description]
	 * @param  {[type]} url          [description]
	 * @param  {[type]} validation   [description]
	 * @param  {[type]} tableRefresh [description]
	 * @return {[type]}              [description]
	 */
	$.fn.submitFormModalWithAjax = function(url, validation, idTableRefresh, urlDataRefresh, callback){
		const $self = this;
		const form = $self.find('form');

		form.validate(validation);
		if(form.valid()){
			$.ajax({
				url: `/api/${url}`,
				data: form.serialize(),
				method: 'POST',
				dataType: 'json'
			}).done(function(response){
				if(response.status){
					$(`#${idTableRefresh}`).refreshTable(urlDataRefresh);
					$self.modal('hide');
					alertSuccess();
				}else{
					alertError();
					console.error('Error submit form');
				}

				if(callback){
					callback();
				}
			}).fail(function(error){
				alertError();
				console.error('Error submit form: ' + error);
			});
		}
	};

    $.fn.personnelPlugin = function (url, columns){
        $.ajax({
            url: `${url}`,
            method: 'GET',
            dataType: 'json'
        }).done(function(response){
            if(response.status){
                var table = $('#personnel-table').DataTable({
                    treeGrid: {
                        left: 10,
                        expandIcon: '<span>+</span>',
                        collapseIcon: '<span>-</span>'
                    },
                    data: response.data,
                    columns: columns,
                    scrollY: 400,
                    scrollCollapse: true,
                    pageLength:10,
                    bPaginate: true,
                    bLengthChange: true,
                    bFilter: true,
                    bInfo: true,
                    language: {
                        emptyTable: 'Không có kết quả nào được tìm thấy !!!',
                        search: 'Tìm Kiếm',
                        info: "Tổng cộng",
                        paginate: {
                            first :'Trang đầu',
                            last:'Trang cuối',
                            previous: '<',
                            next: '>'
                        },
                    }
                });
                $('#personnel-table tbody').on('click', 'tr', function () {
                    var data = table.row( this ).data();
                    var result = "";
                    for(let i = 0; i < data.children.length; i++) {
                        result += "Tên khóa học : "+data.children[i].name+"<br>"+"Học phí : " +data.children[i].tuition+"<br>"
                            +"Ngày mở : "+data.children[i].opening_day+"<br>"+"Ngày đến trường : "+data.children[i].school_day+"<br>"
                            +"Phòng học : "+data.children[i].class_room+"<br><hr><br>";
                    }
                    if(data.major == 'Giáo Viên'){
                        $(this).attr('data-toggle','modal');
                        $(this).attr('data-target','#myModal');
                        $("#nono").html(result);
                    }
                } );
            }else{
                console.error('Error render table: ' + response.msg);
            }
        }).fail(function(error){
            console.error('Error render table: ' + error);
        });
    };
}(jQuery));

/**
 * [formatDate Format date]
 * @param  {[type]} date [description]
 * @param  {String} type [description]
 * @return {[type]}      [description]
 */
function formatDate(date, type = 'DD/MM/YYYY'){
	return moment(date).format(type);
}

/**
 * [formatTime format Time]
 * @param  {[type]} formatTime  [description]
 * @param  {String} date_format [description]
 * @return {[type]}             [description]
 */
function formatTime(formatTime, timeFormat = 'h:i'){
	return moment(formatTime).format(timeFormat);
}

/**
 * [alertSuccess description]
 * @return {[type]} [description]
 */
function alertSuccess(){
	const alert = $('.alert-notification.alert-success');
	alert.removeClass('d-none');
	alert.fadeIn().fadeOut(5000);
}

/**
 * [alertError description]
 * @return {[type]} [description]
 */
function alertError(){
	const alert = $('.alert-notification.alert-danger');
	alert.removeClass('d-none');
	alert.fadeIn().fadeOut(5000);
}
