$(function () {
	$("#attendance-table").renderTable('/api/attendance/list', [
        { title: 'Hiện đang học', data: null, render: (data, type, row, meta) => `${data.class.name}` },
        { title: 'Giáo viên chủ nhiệm', data: null, render: (data, type, row, meta) => `${data.personnel.first_name} ${data.personnel.last_name}` },
        { title: 'khoa hoc', data: null, render: (data, type, row, meta) => `${data.course.name}` },
		{ title: 'hoc sinh', data: null, render: (data, type, row, meta) => `${row.student.full_name}` },
		{ title: 'status', data: 'status', render: (data) => data === 'absent' ? 'absent' : 'resent' },
        { title: 'note', data: 'note', orderable: false },
		{ title: '', data: null, orderable: false, render: (data) => { 
				return `
                        <span class="btn-label btn-edit" data-toggle="modal" data-target="#attendanceModal" data-type-modal="update" data-id="${data.id}">
                            <i class="far fa-edit"></i>
                        </span>
                        <span class="btn-label ml-2 btn-delete" onClick="deleteAttendance(${data.id})">
                            <i class="fas fa-times-circle"></i>
                        </span>
	                    `;
			} 
		}
	]);

	$('#attendanceModal').on('show.bs.modal', function(evt){
		const self = $(this);
		const form = self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');
		
		if(typeModal === 'update'){
			const idAttendance = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/api/attendance/byId/${idAttendance}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					form.find('input[name="attendance_id"]').val(idttendance);
					form.find(`input[name="status"][value="${data.status}"]`).attr('checked', 'checked');
					form.find('input[name="note"]').val(data.note);
				}
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const self = $(this);
		self.resetFormOfModal();
		const form = self.find('form');
		form.find('input[name="attendance_id"]').val('');
		form.find('input[name="status"][value="absent"]').attr('checked', 'checked');
        form.find('input[name="note"]').val(data.note);
	});

	$('#attendanceModal').find('#modal--btn-submit').on('click', function(evt){
		const attendanceId = $('#attendanceModal').find('form').find('input[name="attendance_id"]').val();

		let url = 'attendance/create';
		if(attendanceId){
			url = 'attendance/update';
		}

		$('#attendanceModal').submitFormModalWithAjax(url, {
			messages: {
				class_id: 'Vui lòng chọn lớp!',
			}
		}, 'attendance-table', 'attendance/list');
	});
}(jQuery));

/**
 * [deleteattendance description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function deleteAttendance(id){
	$.ajax({
		url: `/api/attendance/delete/byId/${id}`,
		method: 'GET',
		dataType: 'json'
	}).done(function(response){
		if(response.status){
			alertSuccess();
			$("#attendance-table").refreshTable('attendance/list');
		}else{
			alertError();
			console.log('Error render table: ' + response.msg);
		}
	}).fail(function(error){
		alertError();
		console.log('Error render table: ' + error);
	});
}