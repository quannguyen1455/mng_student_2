$(function () {
    $("#personnel-table").personnelPlugin('/api/personnel/list', [
        { title: 'First name', data: 'first_name'},
        { title: 'Last name', data: 'last_name'},
        { title: 'Email', data: 'email', orderable: false },
        { title: 'Giới tính', data: 'sex', render: (data) => data === 'male' ? 'Nam' : 'Nữ' },
        { title: 'Ngày sinh', data: 'birth_day', render: (data) => formatDate(data) },
        { title: 'Nghề nghiệp', data: 'major', orderable: false },
        { title: 'Work id', data: null, render: (data, type, row, meta) => `${data.work.name}` },
        { title: 'Facebook', data: 'network_social', orderable: false },
        { title: '', data: null, orderable: false, render: (data) => {
                return `
                        <span class="btn-label btn-edit" data-toggle="modal" data-target="#personnelModal" data-type-modal="update" data-id="${data.id}">
                            <i class="far fa-edit"></i>
                        </span>
                        <span class="btn-label ml-2 btn-delete" onClick="deletePersonal(${data.id})">
                            <i class="fas fa-times-circle"></i>
                        </span>
	                    `;
            }
        },
    ]);

    $('#personnelModal').on('show.bs.modal', function(evt){
        const self = $(this);
        const form = self.find('form');
        const typeModal = $(evt.relatedTarget).attr('data-type-modal');

        if(typeModal === 'update'){
            const idPersonal = $(evt.relatedTarget).attr('data-id');
            $.ajax({
                url: `/api/personnel/byId/${idPersonal}`,
                method: 'GET',
                dataType: 'json'
            }).done(function(response){
                if(response.data){
                    let data = response.data;
                    form.find('input[name="personnel_id"]').val(idPersonal);
                    form.find('input[name="first_name"]').val(data.first_name);
                    form.find('input[name="last_name"]').val(data.last_name);
                    form.find('input[name="email"]').val(data.email);
                    form.find(`input[name="sex"][value="${data.sex}"]`).attr('checked', 'checked');
                    form.find('input[name="birth_day"]').val(data.birth_day);
                    form.find(`select[name="major"] option[value="${data.major}"]`).attr('selected', 'selected');
                    form.find(`select[name="work_id"] option[value="${data.work_id}"]`).attr('selected', 'selected');
                    form.find('input[name="network_social"]').val(data.network_social);
                }
            }).fail(function(error){
                console.log('Error render table: ' + error);
            });
        }
    }).on('hidden.bs.modal', function(evt){
        const self = $(this);
        self.resetFormOfModal();
        const form = self.find('form');
        form.find('input[name="personnel_id"]').val('');
        form.find('input[name="sex"][value="male"]').attr('checked', 'checked');
        form.find('select[name="class_id"] option[value=""]').attr('selected', 'selected');
    });

    $('#personnelModal').find('#modal--btn-submit').on('click', function(evt){
        const personnelId = $('#personnelModal').find('form').find('input[name="personnel_id"]').val();

        let url = 'personnel/create';
        if(personnelId){
            url = 'personnel/update';
        }

        $('#personnelModal').submitFormModalWithAjax(url, {
            messages: {
                first_name: 'Tên không được bỏ trống!',
                last_name: 'Tên không được bỏ trống!',
                email: 'Email không được bỏ trống',
                birth_day: 'Vui lòng chọn ngày sinh!',
                major: 'Vui lòng chọn chuyên ngành!',
                work_id: 'Vui lòng chọn nghề nghiệp!',
            }
        }, 'personnel-table', 'personnel/list');
    });
}(jQuery));

function deletePersonal(id){
    $.ajax({
        url: `/api/personnel/delete/byId/${id}`,
        method: 'GET',
        dataType: 'json'
    }).done(function(response){
        if(response.status){
            alertSuccess();
            $("#personnel-table").refreshTable('personnel/list');
        }else{
            alertError();
            console.log('Error render table: ' + response.msg);
        }
    }).fail(function(error){
        alertError();
        console.log('Error render table: ' + error);
    });
}
