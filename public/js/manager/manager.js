$(function () {
	$("#manager-table").renderTable('/api/manager/list', [
        { title: 'Name', data: 'name', orderable: false },
		{ title: 'UserName', data: 'username', orderable: false },
		{ title: 'Type', data: 'type', orderable: false },
		{ title: '', data: null, orderable: false, render: (data) => { 
				return `
                        <span class="btn-label btn-edit" data-toggle="modal" data-target="#ManagerModal" data-type-modal="update" data-id="${data.id}">
                            <i class="far fa-edit"></i>
                        </span>
                        <span class="btn-label ml-2 btn-delete" onClick="deleteManager(${data.id})">
                            <i class="fas fa-times-circle"></i>
                        </span>
	                    `;
			} 
		}
	]);

	$('#managerModal').on('show.bs.modal', function(evt){
		const self = $(this);
		const form = self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');
		
		if(typeModal === 'update'){
			const idManager = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/api/manager/byId/${idManager}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					form.find('input[name="manager_id"]').val(idManager);
					form.find('input[name="name"]').val(data.name);
					form.find('input[name="password"]').val(data.password);
					form.find('input[name="username"]').val(data.username);
					form.find('input[name="type"]').val(data.type);
					form.find('input[name="active"]').val(data.active);
					form.find('select[name="major_id"] option[value="${data.major_id}"]').attr('selected', 'selected');
				}
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const self = $(this);
		self.resetFormOfModal();
		const form = self.find('form');
		form.find('input[name="manager_id"]').val('');
		form.find('input[name="type"][value="type"]').attr('checked', 'checked');
		form.find('select[name="major_id"] option[value=""]').attr('selected', 'selected');
	});

	$('#managerModal').find('#modal--btn-submit').on('click', function(evt){
		const managerId = $('#managerModal').find('form').find('input[name="manager_id"]').val();

		let url = 'manager/create';
		if(managerId){
			url = 'manager/update';
		}

		$('#managerModal').submitFormModalWithAjax(url, {
			messages: {
				name: 'Họ và tên không được bỏ trống!',
				username: 'chọn chức năng!',
				type: 'không được bỏ trống!',
			}
		}, 'manager-table', 'manager/list');
	});
}(jQuery));

/**
 * [deletemanager description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function deleteManager(id){
	$.ajax({
		url: `/api/manager/delete/byId/${id}`,
		method: 'GET',
		dataType: 'json'
	}).done(function(response){
		if(response.status){
			alertSuccess();
			$("#manager-table").refreshTable('manager/list');
		}else{
			alertError();
			console.log('Error render table: ' + response.msg);
		}
	}).fail(function(error){
		alertError();
		console.log('Error render table: ' + error);
	});
}