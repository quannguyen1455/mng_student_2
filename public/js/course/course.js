var mybutton = document.getElementById("myBtn");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


$(function () {
	$("#course-table").renderTable('/api/course/list', [
		{ title: 'Tên khóa học', data: 'name', orderable: true ,},	
		{ title: 'Học phí', data: 'tuition', orderable: true },
		{ title: 'Tỉ lệ', data: 'ratio', orderable: true },
		{ title: 'Link', data: 'link', orderable: false },
		{ title: 'Lớp', data:'class_room', render: (data, type, row, meta) => `${row.class.name}` },
		{ title: 'Giáo viên chủ nhiệm', data:'teacher_id', render: (data, type, row, meta) => `${row.personnel.first_name} ${row.personnel.last_name}` },
		{ title: 'Ngày khai giảng',  data: 'opening_day', render: (data) => formatDate(data) },
		{ title: 'Ngày đến trường',  data: 'school_day', render: (data) => formatDate(data) },
		{ title: 'Thời gian đến trường', orderable: false, data: ['course_week'], render: (data, type, row, meta) => {
				var str = "";
				row.course_week.forEach(function(obj) {
					str += obj.week_name + " | Từ " + obj.from_time + " đến " + obj.to_time + "<br/>" + "<br/>" ;
				});
				return str;
			},
		},
		{ title: '', data: null, orderable: false, render: (data) => {
			return `
					<span class="btn-label btn-detail" data-toggle="modal" data-target="#courseModal" data-type-modal="detail" data-id="${data.id}">
						<i title="show more" class="fa fa-angle-double-down"></i>
					</span>
					
					<span class="btn-label ml-2 btn-edit" data-toggle="modal" data-target="#courseModal" data-type-modal="update" data-id="${data.id}">
						<i title="edit" class="far fa-edit"></i>
					</span>
					
					<span class="btn-label ml-2 btn-delete" onClick="deleteCourse(${data.id})">
						<i title="delete" class="far fa-trash-alt"></i>
					</span>

					`;
			}
		} 
	]);

	$('#courseModal').on('show.bs.modal', function(evt){
		const $self = $(this);
		const $form = $self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');

		const $formRowHide = $('.form-row.hide');
		const $btnSubmit = $('#modal--btn-submit');
		const $courseModalLabel = $('#courseModalLabel');

		if (typeModal === 'detail') {
			$formRowHide.hide();
			$btnSubmit.hide();
			$courseModalLabel.html('Thông báo');
			$('#block').attr('readonly', true);
		} else {
			$formRowHide.show();
			$btnSubmit.show();
			if(typeModal === 'update'){
				$courseModalLabel.html('Update');
			} else {
				$courseModalLabel.html('New');
			}
		}
		
		if(typeModal === 'update' || typeModal === 'detail') {
			const idCourse = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/api/course/byId/${idCourse}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					$form.find('input[name="id"]').val(idCourse);
					$form.find('input[name="name"]').val(data.name);
					$form.find('input[name="tuition"]').val(data.tuition);
					$form.find('input[name="ratio"]').val(data.ratio);
					$form.find('input[name="link"]').val(data.link);
					$form.find(`select[name="teacher_id"] option[value="${data.teacher_id}"]`).attr('selected', 'selected');
					$form.find('input[name="opening_day"]').val(data.opening_day);
					$form.find(`select[name="class_room"] option[value="${data.class_room}"]`).attr('selected', 'selected');
					$form.find('input[name="school_day"]').val(data.school_day);
					

					if (data.course_week.length > 0) {	
						let str = "";
						data.course_week.forEach(function(obj) {							
							$form.find('input#week_' + obj.week_id).attr('checked', 'checked');				
							$form.find('input[name="from_time['+ obj.week_id +']"]').val(obj.from_time);
							$form.find('input[name="to_time['+ obj.week_id +']"]').val(obj.to_time);

							str += obj.week_id + " : " + obj.from_time + " to " + obj.to_time + " " ;
						});
						$form.find('textarea[name="time"]').val(str);

					} else {
						$form.find('input#week_' + obj.week_id).attr(false,false);
						$form.find('input[name="from_time['+ obj.week_id +']"]').val();
						$form.find('input[name="to_time['+ obj.week_id +']"]').val();
					}
				} 
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const $self = $(this);
		$self.resetFormOfModal();
		const $form = $self.find('form');
		$form.find('input[name="id"]').val('');
	});

	$('#courseModal').find('#modal--btn-submit').on('click', function(evt){
		const courseId = $('#courseModal').find('form').find('input[name="id"]').val();

		let url = 'course/create';
		if(courseId){
			url = 'course/update';
		}

		$('#courseModal').submitFormModalWithAjax(url, {
			messages: {
				name: 'Tên khóa học không được rỗng',
				tuition: 'Học phí không được rỗng',
				ratio:'Tỉ lệ không được rỗng',
				link: 'Link không được rỗng',
				class_room:'Lớp học không được rỗng',
				school_day:'Ngày đến trường không được rỗng',
				opening_day:'Ngày khai giảng không được rỗng',
				teacher_id:'Tên giáo viên không được rỗng',
				course_week:'*',
			}
		}, 'course-table', 'course/list');
		
	});
}(jQuery));

/**
 * [deleteCourse description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function deleteCourse(id){
	if( confirm('Xóa khóa học này khỏi danh sách?') == true) {

	$.ajax({
		url: `/api/course/delete/byId/${id}`,
		method: 'GET',
		dataType: 'json'
	}).done(function(response){
		if(response.status){
			alertSuccess();
			$("#course-table").refreshTable('course/list');
		}else{
			alertError();
			console.log('Error render table: ' + response.msg);
		}
	}).fail(function(error){
		alertError();
		console.log('Error render table: ' + error);
	});
	} else {
		;
	}
}


