
/**
 * Nút go to Top
 */
var mybutton = document.getElementById("myBtn");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

$(function () {
	$("#student-table").renderTable('/api/student/list', [
		{ title: 'Họ và Tên', data: null, render: (data, type, row, meta) => `${row.full_name}` },
		{ title: 'Ngày sinh', data: 'birth_day', render: (data) => formatDate(data) },
		{ title: 'Giới tính', data: 'sex', render: (data) => data === 'male' ? 'Nam' : 'Nữ'  },
		{ title: 'Điện thoại', data: 'phone_number', orderable: false },
		{ title: 'Hiện đang học', data: null, render: (data, type, row, meta) => `${data.class.name}` },
		{ title: 'Facebook', data: 'social_network', orderable: false },
		{ title: 'Phụ huynh', data: 'guardian_name' },
		{ title: '', data: null, orderable: false, render: (data) => {
				return `
                        <span class="btn-label btn-edit" data-toggle="modal" data-target="#studentModal" data-type-modal="update" data-id="${data.id}">
                            <i class="far fa-edit"></i>
                        </span>
                        <span class="btn-label ml-2 btn-delete" onClick="deleteStudent(${data.id})">
							<i title="delete" class="far fa-trash-alt"></i>
                        </span>
	                    `;
			} 
		}
	]);

	$('#studentModal').on('show.bs.modal', function(evt){
		const self = $(this);
		const form = self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');

		if(typeModal === 'update'){
			const idStudent = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/api/student/byId/${idStudent}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					form.find('input[name="student_id"]').val(idStudent);
					form.find('input[name="full_name"]').val(data.full_name);
					form.find('input[name="birth_day"]').val(data.birth_day);
					form.find('input[name="phone_number"]').val(data.phone_number);
					form.find(`input[name="sex"][value="${data.sex}"]`).attr('checked', 'checked');
					form.find(`select[name="class_id"] option[value="${data.class_id}"]`).attr('selected', 'selected');
					form.find('input[name="social_network"]').val(data.social_network);
					form.find('input[name="guardian_name"]').val(data.guardian_name);
				}
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const self = $(this);
		self.resetFormOfModal();
		const form = self.find('form');
		form.find('input[name="student_id"]').val('');
		form.find('input[name="sex"][value="male"]').attr('checked', 'checked');
		form.find('select[name="class_id"] option[value=""]').attr('selected', 'selected');
	});

	$('#studentModal').find('#modal--btn-submit').on('click', function(evt){
		const studentId = $('#studentModal').find('form').find('input[name="student_id"]').val();

		let url = 'student/create';
		if(studentId){
			url = 'student/update';
		}

		$('#studentModal').submitFormModalWithAjax(url, {
			messages: {
				full_name: 'Họ và tên không được bỏ trống!',
				birth_day: 'Vui lòng chọn ngày tháng năm sinh!',
				phone_number: 'Số điện thoại không được bỏ trống!',
				class_id: 'Vui lòng chọn lớp!',
				guardian_name: 'Tên phụ huynh không được bỏ trống!'
			}
		}, 'student-table', 'student/list');
	});
}(jQuery));

/**
 * [deleteStudent description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function deleteStudent(id){
	if ( confirm('Xóa học sinh này khỏi danh sách?') == true) {
	$.ajax({
		url: `/api/student/delete/byId/${id}`,
		method: 'GET',
		dataType: 'json'
	}).done(function(response){
		if(response.status){
			alertSuccess();
			$("#student-table").refreshTable('student/list');
		}else{
			alertError();
			console.log('Error render table: ' + response.msg);
		}
	}).fail(function(error){
		alertError();
		console.log('Error render table: ' + error);
	});
	} else {
		;
	}
}
