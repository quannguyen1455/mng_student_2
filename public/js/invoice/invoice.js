
/**
 * Nút go to Top
 */
var mybutton = document.getElementById("myBtn");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
/**
 * Danh sách Invoice
 */
function format ( d ) {	
	//<th>Ngày sinh : ${d.birth_day ? d.birth_day :'Chưa có dữ liệu'}</th>
	var div = 
		`<table class="child-table" style="margin-left:80px ; margin-top:5px !important">
			<thead>
				<tr>
				<th>STT Hóa đơn</th>
				<th>Khóa học</th>
				<th>Lớp học</th>
				<th>Giáo viên chủ nhiệm</th>
				<th>Học phí</th>
				<th>Kỳ hạn hóa đơn</th>
				<th>Tải mẫu hóa đơn</th>
				<th>Hóa đơn</th>
				<th></th>
				</tr>
			</thead>
		<tbody>`;
	d.invoice.forEach(function (item,index){	
		if(item.index !== "") {
		div += 
			`<tr>
				<th>${(index)+1}</th>
				<th>${item.course_name}</th>
				<th>${d.class.name}</th>
				<th>${item.f_n + ' ' + item.l_n }</th>
				<th>${item.price}</th>
				<th>${item.expired}</th>
				<th>
					<button  onclick="pdf(${item.id})" class="btn btn-primary" type="button">
						<i title="print" class="fa fa-download" aria-hidden="true"></i>
					</button>
				</th>
				<th>
					<button  onclick="printById(${item.id})" class="btn btn-primary" type="button">
						<i title="print" class="fa fa-credit-card" aria-hidden="true"></i>
					</button>
				</th>
				<th>
					<button class="btn btn-delete" type="button" onClick="deleteInvoice(${item.id})">
						<i title="delete" class="far fa-trash-alt" aria-hidden="true"></i>
					</button>
				</th>
			</tr>
			`; 
			$(div).addClass( 'loading' ); 
		} if (item.index == '') {
			$(div).addClass( 'loading' ).text("Chưa có dữ liệu!"); 
		}
	});
	div += '</tbody></table>';
	return div; 
}
 
$(function() {
    var dt = $('#invoice-table').DataTable( {
		ajax: "/api/invoice/list",
		scrollY: 400,
        scrollCollapse: true,
		paging: true,
		language: {
			emptyTable: 'Không có kết quả nào được tìm thấy !!!',
			search: 'Tìm Kiếm',
			info: "Tìm thấy _TOTAL_ kết quả",
			paginate: {
			  first :'Trang đầu',
			  last:'Trang cuối',
			  previous: '<',
			  next: '>'
			}
		},
		columns: [	
			{
				data: null,
				render: (data) => `<span class ="details-control ${data.invoice.length > 0 ? 'has-icon' : ''}"></span>`
			},
			{ title: 'Họ tên', data: 'full_name'},
			{ title: 'Ngày sinh', data: 'birth_day'},
			{ title: 'Giới tính', data: 'sex', render: (data) => data === 'male' ? 'Nam' : 'Nữ' },
			{ title: 'Điện thoại', data: 'phone_number', orderable: false },

		]
	} );
	var detailRows = [];
 
    $('#invoice-table tbody').on( 'click', 'tr td span.details-control ', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
		var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
			row.child.hide();
            detailRows.splice( idx, 1 );
        }
        else {
			tr.addClass( 'details' );
			row.child( format( row.data() ) ).show();
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );

	$('#invoiceModal').on('show.bs.modal', function(evt){
		const self = $(this);
		const form = self.find('form');
		const typeModal = $(evt.relatedTarget).attr('data-type-modal');

		
		if(typeModal === 'update') {
			const idInvoice = $(evt.relatedTarget).attr('data-id');
			$.ajax({
				url: `/api/invoice/byId/${idInvoice}`,
				method: 'GET',
				dataType: 'json'
			}).done(function(response){
				if(response.data){
					let data = response.data;
					form.find('input[name="id"]').val(idInvoice);
					form.find('input[name="invoice_date"]').val(data.invoice_date);
					form.find(`select[name="student_id"] option[ value= "${data.student_id}"]`).attr('selected', 'selected');
					form.find('input[name="content"]').val(data.content);
					form.find('input[name="note"]').val(data.note);
					form.find('input[name="biller"]').val(data.biller);	
				} 
			}).fail(function(error){
				console.log('Error render table: ' + error);
			});
		}
	}).on('hidden.bs.modal', function(evt){
		const self = $(this);
		self.resetFormOfModal();
		const form = self.find('form');
		form.find('input[name="id"]').val('');
	});

	$('#invoiceModal').find('#modal--btn-submit').on('click', function(evt){
		const invoiceId = $('#invoiceModal').find('form').find('input[name="id"]').val();
		let url = 'invoice/create';
		alert('Chức năng chưa hoàn thành!');
		if(invoiceId){
			url = 'invoice/update';
			alert('Chức năng chưa hoàn thành!');
		}

		$('#invoiceModal').submitFormModalWithAjax(url, {
			messages: {
			}
		}, 'invoice-table', 'invoice/list');
		
	});
	
}(jQuery));

function deleteInvoice(id){

	if( confirm('Xóa hóa đơn này?') == true) {
		$.ajax({
			url: `/api/invoice/delete/byId/${id}`,
			method: 'GET',
			dataType: 'json'
		}).done(function(response){
				alertSuccess();
				$("#invoice-table").refreshTable('invoice/list');

		}).fail(function(error){
			alertError();
			console.log('Error render table: ' + error);
		});
	} else {
		;
	}
}

//hóa đơn
function pdf(id) {
		if(confirm(':)') == true) {
			return	window.open('/api/invoice/in-pdf', '_blank');
		}else {
			;
		}
}

function printById(id) {
		if(confirm("Xem hóa đơn ?") == true) {
			return	window.open(`/api/invoice/print/${id}`, '_blank');
		}else{
			;
		}
}

function printPage() { 
	if(confirm("In hóa đơn ?") == true) {
		window.print();
	}else{
		;
	} 
}

function invoiceNextMonth() {
	var now = new Date(); 
	var year = now.getFullYear();
	var month = now.getMonth()+1+1; 
	var day = now.getDate();
	
	if(month == 13) {
		year = year+1;
		month = 1;
	}
	var dateTime = day+'/'+month+'/'+year;
	$("#getNextMonth").text(dateTime);

}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		 var c = ca[i];
		 while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		 if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function addMonths(date, months) {
    var d = date.getDate();
    date.setMonth(date.getMonth() + +months);
    if (date.getDate() != d) {
      date.setDate(0);
    }
    return date;
}
function t() {
	var dateSrt=new Date(2016, 7, 30);

	for (var i = 1; i<=12; i++) {
		if (i == 1) {
			dateSrt.setMonth(dateSrt.getMonth());
		} else {
			dateSrt.setMonth(dateSrt.getMonth() + 1);
		}
		var txtDay = formatDate('dd-mm-yy', dateSrt);

		$("#demo").append("<label>" + txtDay + "</label><br>");
	}
}





