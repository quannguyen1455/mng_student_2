<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| 
*/

Route::get('/student/list', 'Api\StudentController@index');
Route::get('/student/byId/{id}', 'Api\StudentController@detail');
Route::post('/student/create', 'Api\StudentController@create');
Route::post('/student/update', 'Api\StudentController@update');
Route::get('/student/delete/byId/{id}', 'Api\StudentController@delete');

//course
Route::get('/course/list', 'Api\CourseController@index');
Route::get('/course/byId/{id}', 'Api\CourseController@detail');
Route::post('/course/create', 'Api\CourseController@create');
Route::post('/course/update', 'Api\CourseController@update');
Route::get('/course/delete/byId/{id}', 'Api\CourseController@delete');

Route::get('/personnel/list', 'Api\PersonnelController@index');
Route::get('/personnel/byId/{id}', 'Api\PersonnelController@detail');
Route::post('/personnel/create', 'Api\PersonnelController@create');
Route::post('/personnel/update', 'Api\PersonnelController@update');
Route::get('/personnel/delete/byId/{id}', 'Api\PersonnelController@delete');

//export file
Route::get('/export/get_pdf', 'Api\PdfController@get_pdf');
Route::get('/export/post_pdf', 'Api\PdfController@post_pdf');
Route::get('/export/excel','Api\ExcelController@index');

//invoice
Route::get('/invoice/list', 'Api\InvoiceController@index');
Route::get('/invoice/in-pdf', 'Api\InvoiceController@inpdf');
Route::get('/student/export', 'Api\StudentController@export');
Route::post('/invoice/create', 'Api\InvoiceController@create');
Route::post('/invoice/update', 'Api\InvoiceController@update');
Route::get('/invoice/delete/byId/{id}', 'Api\InvoiceController@delete');
Route::get('/invoice/in-pdf', 'Api\InvoiceController@inpdf');
Route::get('/invoice/print/{id}', 'Api\InvoiceController@inpdfid');

//admin
Route::get('/manager/list', 'Api\ManagerController@index');
Route::get('/manager/byId/{id}', 'Api\ManagerController@detail');
Route::post('/manager/create', 'Api\ManagerController@create')->middleware(['can:permission']);
Route::post('/manager/update', 'Api\ManagerController@update')->middleware(['can:permission']);
Route::get('/manager/delete/byId/{id}', 'Api\ManagerController@delete')->middleware(['can:permission']);

//attendance
Route::get('/attendance/list', 'Api\AttendanceController@index');
Route::get('/attendance/byId/{id}', 'Api\AttendanceController@detail');
Route::post('/attendance/create', 'Api\AttendanceController@create');
Route::post('/attendance/update', 'Api\AttendanceController@update');
Route::get('/attendance/delete/byId/{id}', 'Api\AttendanceController@delete');