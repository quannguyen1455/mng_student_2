<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Auth::routes(['register' => false]);
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('home');

//list-course
Route::get('/course/list', 'CourseController@list')->name('course-list');

//Personnel
Route::get('/personnel/list', 'PersonnelController@list')->name('personnel-list');

//list-student
Route::get('/student/list', 'StudentController@list')->name('student-list');

//list-course
Route::get('/course/list', 'CourseController@list')->name('course-list');

//list-invoice
Route::get('/invoice/list', 'InvoiceController@list')->name('invoice-list');
Route::get('/invoice/get-pdf','InvoiceController@get_pdf)');
Route::get('/invoice/post-pdf','InvoiceController@post_pdf');

//print-pdf
Route::get('/print-pdf','PdfController@print_pdf');
Route::get('/down-pdf','PdfController@down_pdf');
//admin
Route::get('/manager/list', 'ManagerController@list')->name('manager-list');
//attendance
Route::get('/attendance/list', 'AttendanceController@list')->name('attendance-list');
