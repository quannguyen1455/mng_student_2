<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('invoice_id')->nullable(false);
            $table->unsignedInteger('fee_id')->nullable(false);
            $table->unsignedInteger('course_id')->nullable(false);
            $table->decimal('price', 9, 4)->nullable(false);
            $table->timestamp('expired')->nullable(false);
            $table->text('note')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
