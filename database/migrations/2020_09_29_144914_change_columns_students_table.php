<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function( Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('guardian_id');
            $table->string('social_network', 250)->nullable()->change();
            $table->decimal('liabilities', 9, 4)->nullable()->change();
            $table->text('note')->nullable()->change();
            $table->string('guardian_name', 250)->nullable()->after('social_network');
            $table->string('full_name', 250)->nullable(false)->after('id');
            $table->string('phone_number', 20)->nullable()->after('guardian_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
