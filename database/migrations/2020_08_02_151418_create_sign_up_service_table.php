<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignUpServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_up_service', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('student_id')->nullable(false);
            $table->unsignedInteger('service_id')->nullable(false);
            $table->decimal('fee', 9, 4)->nullable(false);
            $table->decimal('price', 9, 4)->nullable(false);
            $table->text('content')->nullable(false);
            $table->timestamp('date_start')->nullable(true);
            $table->timestamp('date_end')->nullable(true);
            $table->boolean('enable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sign_up_service');
    }
}
