<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_up', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('course_id')->nullable(false);
            $table->unsignedInteger('student_id')->nullable(false);
            $table->timestamp('date_start')->nullable(true);
            $table->decimal('tuition', 9, 4)->nullable(false);
            $table->timestamp('date_end')->nullable(true);
            $table->boolean('enable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sign_up');
    }
}
