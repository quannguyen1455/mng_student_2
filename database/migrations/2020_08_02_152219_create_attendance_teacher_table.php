<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_teacher', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('teacher_id')->nullable(false);
            $table->unsignedInteger('class_id')->nullable(false);
            $table->unsignedInteger('course_id')->nullable(false);
            $table->unsignedInteger('student_id')->nullable(false);
            $table->text('note');
            $table->enum('status', ['absent', 'resent']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_teacher');
    }
}
