<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name', 250)->nullable(false);
            $table->decimal('tuition', 9, 4)->nullable(false);
            $table->float('ratio', 9, 2);
            $table->boolean('link')->defaultValue(false);
            $table->unsignedInteger('teacher_id')->nullable(false);
            $table->date('opening_day');
            $table->date('school_day');
            $table->string('time', 100);
            $table->string('class_room', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
