<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id')->nullable(false);
            $table->unsignedInteger('week_id')->nullable(false);
            $table->string('from_time', 100);
            $table->string('to_time', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_weeks');
    }
}
