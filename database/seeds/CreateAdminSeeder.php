<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {   
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            'name'      => 'Super Admin',
            'username'  => 'super admin',
            'password'  => Hash::make('admin'),
            'type'      => 1
        ]);

        DB::table('admins')->insert([
            'name'      => 'Admin',
            'username'  => 'admin',
            'password'  => Hash::make('admin'),
            'type'      => 2
        ]);

        DB::table('admins')->insert([
            'name'      => 'Manager',
            'username'  => 'manager',
            'password'  => Hash::make('admin'),
            'type'      => 3
        ]);

        DB::table('admins')->insert([
            'name'      => 'User',
            'username'  => 'user',
            'password'  => Hash::make('admin'),
            'type'      => 4
        ]);
    }
}
