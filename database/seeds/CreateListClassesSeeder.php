<?php

use Illuminate\Database\Seeder;

class CreateListClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->truncate();
        DB::table('classes')->insert([
        	[ 'name' => 'Mầm Non' ],
        	[ 'name' => 'Lớp 1' ],
        	[ 'name' => 'Lớp 2' ],
        	[ 'name' => 'Lớp 3' ],
        	[ 'name' => 'Lớp 4' ],
        	[ 'name' => 'Lớp 5' ],
        	[ 'name' => 'Lớp 6' ],
        	[ 'name' => 'Lớp 7' ],
        	[ 'name' => 'Lớp 8' ],
        	[ 'name' => 'Lớp 9' ],
        	[ 'name' => 'Lớp 10' ],
        	[ 'name' => 'Lớp 11' ],
        	[ 'name' => 'Lớp 12' ],
        	[ 'name' => 'Lớp Khác']
        ]);
    }
}
