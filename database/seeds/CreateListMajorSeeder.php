<?php

use Illuminate\Database\Seeder;

class CreateListMajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('major')->truncate();
        DB::table('major')->insert([
        	[ 'name' => 'Giáo Viên' , 'description' => NULL ],
            [ 'name' => 'Kế Toán', 'description' => NULL ],
        	[ 'name' => 'Bảo Vệ', 'description' => NULL ],
        	[ 'name' => 'NVVP', 'description' => 'Nhân Viên Văn Phòng' ],
        	[ 'name' => 'IT', 'description' => 'Nhân Viên IT' ],
        	[ 'name' => 'Giúp Việc', 'description' => NULL ]
        ]);
    }
}
